from django.http import request
from django.shortcuts import render
from django.views.generic import ListView, DetailView

from .models import IceCream, IceCreamCategory


class IceCreamListView(ListView):

    queryset = IceCream.objects.all()


class IceCreamDetail(DetailView):

    model = IceCream
    context_object_name = 'icecream'



class CategoryListView(ListView):

    queryset = IceCreamCategory.objects.all()


class IceCreamByCategory(ListView):

    model = IceCream

    def get_queryset(self):
        category = self.request.path.split('/')[-2]
        if category:
            return self.model.objects.filter(categories__id=category)
        return super().get_queryset()