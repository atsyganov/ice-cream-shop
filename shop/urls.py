from django.urls import path

from . import views

urlpatterns = [
    path('index/', views.IceCreamListView.as_view(), name='icecream-list'),
    path('<uuid:pk>/', views.IceCreamDetail.as_view(), name='icecream-detail'),
    path('categories/', views.CategoryListView.as_view(), name='category-list'),
    path('by-category/<uuid:pk>/', views.IceCreamByCategory.as_view(), name='by_category')

]
