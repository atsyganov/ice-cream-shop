import uuid

from django.db import models


class TimeStampedModel(models.Model):

    class Meta:

        abstract = True

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class IceCream(TimeStampedModel):

    title = models.CharField(max_length=100)
    categories = models.ManyToManyField('IceCreamCategory', related_name='ice_creams', blank=True)
    shop = models.ForeignKey('Shop', on_delete=models.CASCADE)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.title


class IceCreamCategory(TimeStampedModel):

    title = models.CharField(max_length=100)
    description = models.TextField(blank=True)


    def __str__(self):
        return self.title


class Shop(TimeStampedModel):

    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title
