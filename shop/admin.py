from django.contrib import admin

from .models import (
    Shop, IceCream, IceCreamCategory
)


@admin.register(Shop)
class ShopAdmin(admin.ModelAdmin):

    pass


@admin.register(IceCreamCategory)
class IceCreamCategoryAdmin(admin.ModelAdmin):

    pass


@admin.register(IceCream)
class IceCreamAdmin(admin.ModelAdmin):

    pass
